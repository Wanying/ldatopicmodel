#This project is used to realize the LDA model
#
# @author Wanying Ding
# @orgniaztion: Drexel University
# @email: wd78@drexel.edu
#
#
#The input needs two files:
#	(1) Word　Map File:
#		format like:
#		Word1:ID1
#		Word2:ID2
#	This file should cover all the words in your document
#	(2) Document File:
#	    in this file, I read a json file with document id and document text in each document.
#	    and each line is an independent json object
#	    If you want to change the input, please change the file read code in algorithm.Model.loadDocument part