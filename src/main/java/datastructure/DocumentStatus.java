/**
 * 
 */
package datastructure;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @date Jan 26, 2015
 *
 *
 *This class is used define the document status
 */
public class DocumentStatus {

	private int did;
	private int length;
	public int topicAssignment[];
	public WordStatus wordList[];
	
	public DocumentStatus(int did, int length, int numberOfTopic){
		this.did=did;
		this.length=length;
		this.topicAssignment=new int[numberOfTopic];
		this.wordList=new WordStatus[length];
	}
	
	public int getDocumentId(){
		return did;
	}

	public int getLength() {
		return length;
	}
}
