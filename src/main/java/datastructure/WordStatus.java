/**
 * 
 */
package datastructure;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @date Jan 26, 2015
 *
 *
 *This class is used to define the word status in LDA
 */
public class WordStatus {
	private int wid;
	private int topicAssignment;
	
	public WordStatus(int wid){
		this.wid=wid;
		this.topicAssignment=-1;
	}
	
	public int getWordId(){
		return wid;
	}

	public int getTopicAssignment() {
		return topicAssignment;
	}

	public void setTopicAssignment(int topicAssignment) {
		this.topicAssignment = topicAssignment;
	}
}
