/**
 * 
 */
package algorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @date Jan 26, 2015
 *
 *
 * This class is used generate the vocabularies for use
 * the input should be a wordmap file
 * like:
 * apple:1
 * banana:2
 * ...
 */
public class Vocabulary {

	HashMap<String,Integer> W2ID = new HashMap<String,Integer>();
	HashMap<Integer,String> ID2W = new HashMap<Integer,String>();
	
	public Vocabulary(String wordMapFile) throws IOException{
		readVocabulary(wordMapFile);
	}
	
	public void readVocabulary(String fileName) throws IOException{
		Scanner scanner = new Scanner(new FileReader(new File(fileName)));
		while(scanner.hasNext()){
			String line = scanner.nextLine();
			String terms[]= line.split(":");
			String word= terms[0].trim();
			int id = Integer.parseInt(terms[1].trim());
			W2ID.put(word, id);
			ID2W.put(id, word);
		}
		scanner.close();
	}

	public HashMap<String, Integer> getW2ID() {
		return W2ID;
	}

	public HashMap<Integer, String> getID2W() {
		return ID2W;
	}
	
}
