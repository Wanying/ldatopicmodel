/**
 * 
 */
package algorithm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import datastructure.DocumentStatus;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @date Jan 26, 2015
 *
 *
 *This class is used to as the model after gibbs Sampling
 */
public class SaveModel {

	/**
	 * @param args
	 */
	HashMap<Integer,String> ID2W;
	int[][] twc;
	int[]tc;
	int[][] dtc;
	int[] dc;
	
	int numberOfDocuments;
	int numberOfWords;
	int numberOfTopics;
	
	List<DocumentStatus> documents;
	
	double alpha;
	double beta;
	
	public SaveModel(GibbsSampling gs, Model model){
		this.ID2W=model.ID2W;
		this.numberOfDocuments=model.numberOfDocuments;
		this.numberOfWords=model.numberOfWords;
		this.numberOfTopics=model.numberOfTopics;
		
		this.twc=gs.twc;
		this.tc=gs.tc;
		this.dtc=gs.dtc;
		this.dc=gs.dc;
		
		this.documents=gs.documents;
	}
	
	public void saveDocumentTopicDistribution(String fileName) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
		for(int d=0;d<numberOfDocuments;d++){
			bw.write(documents.get(d).getDocumentId()+"\t");
			for(int t=0;t<numberOfTopics;t++){
				double theta = (dtc[d][t]+alpha)/(dc[d]+numberOfTopics*alpha);
				bw.write(theta+"\t");
			}
			bw.newLine();
			bw.flush();
		}
		bw.close();
	}
	
	public void saveTopicWordDistribution(String fileName) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
		for(int t=0;t<numberOfTopics;t++){
			for(int w=0;w<numberOfWords;w++){
				double phi = (twc[t][w]+beta)/(tc[t]+numberOfWords*beta);
				bw.write(phi+"\t");
			}
			bw.newLine();
			bw.flush();
		}
		bw.close();
	}
	
	public void saveTopWords(String fileName, int top) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
		for(int t=0;t<numberOfTopics;t++){
			bw.write("Topic "+t+"\n");
			List<ItemScore> list = new ArrayList<ItemScore>();
			for(int w=0;w<numberOfWords;w++){
				ItemScore is = new ItemScore(w,twc[t][w]);
				list.add(is);
			}
			Collections.sort(list);
			for(int w=0;w<top;w++){
				ItemScore is = list.get(w);
				int wid = is.wid;
				double score = is.score;
				String word = ID2W.get(wid);
				bw.write(word+":"+score+"\n");
				bw.flush();
			}
		}
		bw.flush();
		bw.close();
	}
	
	public static void main(String[] args) throws IOException {
		String wordMap = "C:/Project/PopularityPrediction/PythonFiles/data/wordMap.txt";
		String movieList= "C:/Project/PopularityPrediction/PythonFiles/data/movieList.txt";
		
		String thetaFile = "C:/Project/PopularityPrediction/PythonFiles/data/theta.txt";
		String phiFile ="C:/Project/PopularityPrediction/PythonFiles/data/phi.txt";
		String topFile="C:/Project/PopularityPrediction/PythonFiles/data/top.txt";
		
		int numberOfTopic=50;
		double alpha=0.2;
		double beta =0.1;
		int iter=2000;
		
		Model model = new Model(numberOfTopic, wordMap, movieList);
		GibbsSampling gs = new GibbsSampling(model,alpha,beta,iter);
		SaveModel sm = new SaveModel(gs,model);
		
		sm.saveDocumentTopicDistribution(thetaFile);
		sm.saveTopicWordDistribution(phiFile);
		sm.saveTopWords(topFile, 50);
		

	}

}

class ItemScore implements Comparable{
	int wid;
	double score;
	
	public ItemScore(int wid, double score){
		this.wid=wid;
		this.score=score;
	}
	
	public int compareTo(Object obj){
		
		ItemScore is = (ItemScore)obj;
		if(is.score>this.score) return 1;
		else if(is.score==this.score) return 0;
		else return -1;
		
	}
}
