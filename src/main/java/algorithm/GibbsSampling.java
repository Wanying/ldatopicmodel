/**
 * 
 */
package algorithm;

import java.util.List;
import java.util.Random;

import datastructure.DocumentStatus;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @date Jan 26, 2015
 *
 *This class is to use Gibbs Sampling to  infer the topic distribution
 */
public class GibbsSampling {
	
	
	public double alpha;
	public double beta;
	
	public int twc[][];
	public int dtc[][];
	public int tc[];
	public int dc[];
	
	public int numberOfDocuments;
	public int numberOfWords;
	public int numberOfTopics;
	
	public List<DocumentStatus> documents;
	
	private Random random;
	private double pk[];
	private double pSum;
	private double u;
	
	public GibbsSampling(Model model,double alpha, double beta,int iter){
		this.alpha=alpha;
		this.beta=beta;
		this.twc=model.twc;
		
		this.dtc=model.dtc;
		this.tc=model.tc;
		this.dc=model.dc;
		
		this.numberOfDocuments=model.numberOfDocuments;
		this.numberOfTopics = model.numberOfTopics;
		this.numberOfWords=model.numberOfWords;
		
		this.documents=model.documents;
		
		this.pk=new double[numberOfTopics];
		this.pSum=-1;
		this.u=-1;
		this.random= new Random();
		
		sampling(iter);
	}
	
	private void sampling(int iter){
		for(int i=0;i<iter;i++){
			System.out.println("IS SAMPLING "+i+"/"+iter+" ...");
			for(int d=0;d<documents.size();d++){
				for(int w=0;w<documents.get(d).getLength();w++){
					decrement(d,w);
					int topic=topicSample(d,w);
					allocate(d,w,topic);
				}
			}
		}
	}
	
	private void decrement(int d, int w){
		int topicAssignment = documents.get(d).wordList[w].getTopicAssignment();
		int wid = documents.get(d).wordList[w].getWordId();
		
		twc[topicAssignment][wid]--;
		tc[topicAssignment]--;
		dtc[d][topicAssignment]--;
		dc[d]--;
	}
	
	private void allocate(int d, int w, int topicAssignment){
		int wid = documents.get(d).wordList[w].getWordId();
		documents.get(d).wordList[w].setTopicAssignment(topicAssignment);
		
		twc[topicAssignment][wid]++;
		tc[topicAssignment]++;
		dtc[d][topicAssignment]++;
		dc[d]++;
	}
	
	private int topicSample(int d, int w){
		int topic=-1;
		this.pSum=0;
		
		int wid = documents.get(d).wordList[w].getWordId();
		
		for(int t=0;t<numberOfTopics;t++){
			double twp=((twc[t][wid]+beta)/(tc[t]+numberOfWords*beta));
			double dtp=((dtc[d][t]+alpha)/(dc[d]+numberOfTopics*alpha));
			if((twp<0)||(dtp<0)){
				System.out.println("twp: "+twp+", dtp: "+dtp);
			}
			pSum+=twp*dtp;
			pk[t]=pSum;
		}
		u=random.nextDouble()*pSum;
		
		for(topic=0;topic<numberOfTopics;topic++){
			if(pk[topic]>u) {
				break;
			}
		}
//		if(topic>=numberOfTopics){
//			System.out.println("Wrong Topic: "+topic+", "+u+", "+pSum);
//		}
		return topic;
	}

}
