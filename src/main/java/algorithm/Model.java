/**
 * 
 */
package algorithm;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.json.JSONObject;

import datastructure.DocumentStatus;
import datastructure.WordStatus;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @date Jan 26, 2015
 *
 *
 * This class is used to initiate the model for the whole project
 */


public class Model {
	public HashMap<String,Integer> W2ID;
	public HashMap<Integer,String> ID2W;
	
	public int numberOfWords;
	public int numberOfDocuments;
	public int numberOfTopics;
	
	public List<DocumentStatus> documents;
	
	public int[][] twc;
	public int[][] dtc;
	public int[]tc;
	public int[]dc;
	
	public Model(int numberOfTopics,String mapFile,String documentFile) throws IOException{
		setNumberOfTopic(numberOfTopics);
		loadDictionary(mapFile);
		loadDocuments(documentFile);
		
		initVariables();
		initModel();
	}
	
	public void setNumberOfTopic(int numberOfTopics){
		this.numberOfTopics=numberOfTopics;
	}
	
	public void loadDictionary(String mapFile) throws IOException{
		/*
		 * load dictionary from external file
		 * */
		Vocabulary voc = new Vocabulary(mapFile);
		this.W2ID=voc.getW2ID();
		this.ID2W=voc.getID2W();
		this.numberOfWords=W2ID.size();
	}
	
	public void loadDocuments(String documentFile) throws IOException{
		/*
		 * You can change this part of code according to your input format
		 * Here, I read the documents from a JSON file
		 * */
		this.documents = new ArrayList<DocumentStatus>();
		Scanner scanner = new Scanner(new FileReader(new File(documentFile)));
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			JSONObject jsonObj = new JSONObject(line);
			String document = jsonObj.getString("description").trim();
			int id = Integer.parseInt(jsonObj.getString("movieID").trim());
			String words[]= document.split(" ");
			int docLength = words.length;
			// set up one document
			DocumentStatus ds = new DocumentStatus(id,docLength,numberOfTopics);
			for(int w=0;w<words.length;w++){
				String word = words[w].trim();
				int wid = W2ID.get(word);
				//set up each word, and put it into this document
				WordStatus ws = new WordStatus(wid);
				ds.wordList[w]=ws;
			}
			documents.add(ds);
		}
		this.numberOfDocuments=documents.size();
	}
	
	public void initVariables(){
		//count the number of one word in a topic
		this.twc = new int[numberOfTopics][numberOfWords];
		// count the number of words appearing in each words with in a document
		this.dtc = new int[numberOfDocuments][numberOfTopics];
		// count the number of words in each topic
		this.tc= new int[numberOfTopics];
		//count the number of words in each document
		this.dc = new int[numberOfDocuments];
	}
	
	public void initModel(){
		Random rand = new Random();
		for(int d=0;d<numberOfDocuments;d++){
			for(int w=0;w<documents.get(d).wordList.length;w++){
				int topicAssignment = rand.nextInt(numberOfTopics);
				documents.get(d).wordList[w].setTopicAssignment(topicAssignment);
				
				int wid = documents.get(d).wordList[w].getWordId();
				twc[topicAssignment][wid]++;
				tc[topicAssignment]++;
				dtc[d][topicAssignment]++;
				dc[d]++;
			}
		}
	}

}
